package com.example.jade_wine_backstage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_backstage.entity.WarehouseDetail;

/**
 * <p>
 * 仓库中间表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface WarehouseDetailService extends IService<WarehouseDetail> {

}
