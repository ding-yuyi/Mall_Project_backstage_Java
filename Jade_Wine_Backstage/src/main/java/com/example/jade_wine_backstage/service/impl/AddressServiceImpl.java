package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.entity.Address;
import com.example.jade_wine_backstage.mapper.AddressMapper;
import com.example.jade_wine_backstage.service.AddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 用户地址表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
@Slf4j
public class AddressServiceImpl extends ServiceImpl<AddressMapper, Address> implements AddressService {

    @Resource
    private AddressMapper addressMapper;

    /**
     * 根据address_id查询地址
     * 参数:addressId
     */
    @Override
    public Result queryAddressByUserId(Integer addressId) {
        log.info("查询地址,addressId:{}", addressId);
        Address address = addressMapper.selectById(addressId);
        log.info("查询地址{}", address);
        if (null != address) {
            return Result.success(address);
        }
        return Result.fail();
    }
}
