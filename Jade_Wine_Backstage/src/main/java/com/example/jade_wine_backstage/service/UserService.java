package com.example.jade_wine_backstage.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_backstage.controller.form.FindUserForm;
import com.example.jade_wine_backstage.controller.form.LoginForm;
import com.example.jade_wine_backstage.entity.User;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface UserService extends IService<User> {

    User login(LoginForm form);

    //查询普通用户
    Page<User> findUser(FindUserForm form);

    //禁用账户
    Boolean forbiddenUser(Integer userId);

    Boolean deleteUser(Integer userId);
}
