package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.entity.Warehouse;
import com.example.jade_wine_backstage.mapper.WarehouseMapper;
import com.example.jade_wine_backstage.service.WarehouseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 仓库中间表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
public class WarehouseServiceImpl extends ServiceImpl<WarehouseMapper, Warehouse> implements WarehouseService {

}
