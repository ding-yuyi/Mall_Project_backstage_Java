package com.example.jade_wine_backstage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.controller.form.PageForm;
import com.example.jade_wine_backstage.entity.GoodsSku;

/**
 * <p>
 * sku属性键表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface GoodsSkuService extends IService<GoodsSku> {

    /**
     * 分页查询所有商品
     * 参数: pageNum 页码   pageSize 页容量
     */

    public Result pageQueryAllGoodsSku(PageForm form);

    /**
     * 根据id删除goods_sku
     * 参数：skuId
     */
    public Result deleteGoods(Integer skuId);

    /**
     * 根据skuId修改goods_sku
     */
    public Result updateGoodsSkuById(GoodsSku goodsSku);

    /**
     * 新增
     */
    public Result insertGoods(GoodsSku goodsSku);
}
