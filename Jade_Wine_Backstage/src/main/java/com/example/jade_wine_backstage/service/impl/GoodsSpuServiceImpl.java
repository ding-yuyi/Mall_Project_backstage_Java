package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.entity.GoodsSpu;
import com.example.jade_wine_backstage.mapper.GoodsSpuMapper;
import com.example.jade_wine_backstage.service.GoodsSpuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * spu商品表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
@Slf4j
public class GoodsSpuServiceImpl extends ServiceImpl<GoodsSpuMapper, GoodsSpu> implements GoodsSpuService {

    @Resource
    private GoodsSpuMapper goodsSpuMapper;

    /**
     * 获取所有spu
     *
     * @return
     */
    @Override
    public Result queryAllSpuName() {
        log.info("获取所有spuName");
        QueryWrapper<GoodsSpu> wrapper = new QueryWrapper<>();
        List<GoodsSpu> goodsSpus = goodsSpuMapper.selectList(wrapper);
        log.info("获取所有spuName{}", goodsSpus);
        if (null != goodsSpus) {
            return Result.success(goodsSpus);
        }
        return Result.fail();
    }
}
