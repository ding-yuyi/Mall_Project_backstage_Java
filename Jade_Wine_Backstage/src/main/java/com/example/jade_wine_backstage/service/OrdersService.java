package com.example.jade_wine_backstage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.controller.form.PageForm;
import com.example.jade_wine_backstage.entity.Orders;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface OrdersService extends IService<Orders> {

    /**
     * 分页查询全部订单
     * 参数 form: pageNum 页码      pageSize 页容量
     *
     * @return
     */
    public Result queryAllOrders(PageForm form);

    /**
     * 根据id修改订单状态
     * 参数：orderId
     */
    public Result updateOrderState(Integer orderId);

    /**
     * 根据订单号查询订单
     * 参数: orderNum 订单号
     */
    public Result queryOrderByOrderNum(String orderNum);
}
