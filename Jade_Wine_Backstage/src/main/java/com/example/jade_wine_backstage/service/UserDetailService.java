package com.example.jade_wine_backstage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.entity.UserDetail;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface UserDetailService extends IService<UserDetail> {

    /**
     * 根据userId查询昵称
     * 参数:userId
     */
    public Result queryNickNameByUserId(Integer userId);
}
