package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.controller.form.FindUserForm;
import com.example.jade_wine_backstage.controller.form.LoginForm;
import com.example.jade_wine_backstage.entity.User;
import com.example.jade_wine_backstage.mapper.UserMapper;
import com.example.jade_wine_backstage.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public User login(LoginForm form) {
        log.info("开始执行service层,{}", form);
        //根据用户名查找
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", form.getUsername());
        User user = userMapper.selectOne(queryWrapper);
        if (null == user) {
            //根据用邮箱查找
            QueryWrapper<User> queryWrapper1 = new QueryWrapper<>();
            queryWrapper.eq("username", form.getUsername());
            user = userMapper.selectOne(queryWrapper);
            if (null == user) {
                //根据手机查找
                QueryWrapper<User> queryWrapper2 = new QueryWrapper<>();
                queryWrapper.eq("username", form.getUsername());
                user = userMapper.selectOne(queryWrapper);
            }
        }
        log.info("用户信息，{}", user);

        //如果用户不存在，抛出异常
        if (null == user) {
            return null;
        } else {
            if (user.getPassword().equals(form.getPassword()) && (user.getRole() == 1 || user.getRole() == 2)) {
                return user;
            } else {
                return null;
            }
        }
    }

    @Override
    public Page<User> findUser(FindUserForm form) {
        log.info("用户信息,{}", form);

        Page<User> userPage = new Page<>(form.getPageCount(), form.getPageSize());
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        //判断是否是查看管理员

        if (form.getIsManager()) {
            userQueryWrapper.eq("role", 1);
        } else {
            userQueryWrapper.eq("role", 0);
        }


        //如果没有查询用户名，则查询所有普通用户
        if (StringUtils.isEmpty(form.getUsername())) {
            Page<User> page = userMapper.selectPage(userPage, userQueryWrapper);
            return page;
        }

        userQueryWrapper.like("username", form.getUsername());
        Page<User> userPage1 = userMapper.selectPage(userPage, userQueryWrapper);
        return userPage1;
    }

    @Override
    public Boolean deleteUser(Integer userId) {
        int i = userMapper.deleteById(userId);
        if (i > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean forbiddenUser(Integer userId) {
        User user = userMapper.selectById(userId);
        log.info("用户信息,{}", user);
        user.setUserStatus(0);

        int i = userMapper.updateById(user);
        if (i > 0) {
            return true;
        }
        return false;
    }
}
