package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.entity.SkuValue;
import com.example.jade_wine_backstage.mapper.SkuValueMapper;
import com.example.jade_wine_backstage.service.SkuValueService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sku属性值表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
public class SkuValueServiceImpl extends ServiceImpl<SkuValueMapper, SkuValue> implements SkuValueService {

}
