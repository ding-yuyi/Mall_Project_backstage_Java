package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.entity.UserDetail;
import com.example.jade_wine_backstage.mapper.UserDetailMapper;
import com.example.jade_wine_backstage.service.UserDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
@Slf4j
public class UserDetailServiceImpl extends ServiceImpl<UserDetailMapper, UserDetail> implements UserDetailService {

    @Resource
    private UserDetailMapper userDetailMapper;

    /**
     * 根据userId查询昵称
     * 参数:userId
     */

    @Override
    public Result queryNickNameByUserId(Integer userId) {
        log.info("根据userId获得昵称{}", userId);
        QueryWrapper<UserDetail> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        UserDetail userDetail = userDetailMapper.selectOne(wrapper);
        log.info("根据userId获得昵称{}", userDetail);
        if (null != userDetail) {
            return Result.success(userDetail);
        }
        return Result.fail();
    }
}
