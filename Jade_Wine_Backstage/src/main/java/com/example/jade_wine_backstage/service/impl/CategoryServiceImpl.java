package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.entity.Category;
import com.example.jade_wine_backstage.mapper.CategoryMapper;
import com.example.jade_wine_backstage.service.CategoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 类别表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

}
