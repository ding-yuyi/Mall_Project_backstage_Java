package com.example.jade_wine_backstage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.entity.Address;

/**
 * <p>
 * 用户地址表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface AddressService extends IService<Address> {
    /**
     * 根据address_id查询地址
     * 参数:addressId
     */
    public Result queryAddressByUserId(Integer userId);
}
