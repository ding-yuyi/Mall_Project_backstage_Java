package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.controller.form.PageForm;
import com.example.jade_wine_backstage.entity.Orders;
import com.example.jade_wine_backstage.entity.OrdersAndOrderDetail;
import com.example.jade_wine_backstage.mapper.OrdersMapper;
import com.example.jade_wine_backstage.service.OrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
@Slf4j
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {

    @Resource
    private OrdersMapper ordersMapper;


    /**
     * 分页查询全部订单
     * 参数 form: pageNum 页码      pageSize 页容量
     *
     * @return
     */
    @Override
    public Result queryAllOrders(PageForm form) {
        log.info("分页查询所有订单{}", form);
        Page<OrdersAndOrderDetail> ordersAndOrderDetailPage = new Page<>(form.getPageNum(), form.getPageSize());
        IPage<OrdersAndOrderDetail> ordersAndOrderDetailIPage = ordersMapper.pageQueryOrders(ordersAndOrderDetailPage);
        log.info("分页查询所有订单{}", ordersAndOrderDetailIPage);
        if (null != ordersAndOrderDetailIPage) {
            return Result.success(ordersAndOrderDetailIPage);
        }
        return Result.fail();
    }

    /**
     * 根据id修改订单状态
     * 参数：orderId
     */

    @Override
    public Result updateOrderState(Integer orderId) {
        log.info("根据orderId修改状态{}", orderId);
        Integer i = ordersMapper.updateOrderStateByOrderId(orderId);
        log.info("根据orderId修改状态{}", i);
        if (i == 1) {
            return Result.success();
        }
        return Result.fail();
    }

    /**
     * 根据订单号查询订单
     * 参数: orderNum 订单号
     */
    @Override
    public Result queryOrderByOrderNum(String orderNum) {
        log.info("根据orderNum查询order{}", orderNum);
        Page<OrdersAndOrderDetail> page = new Page<>();
        IPage<OrdersAndOrderDetail> iPage = ordersMapper.queryOrder(page, orderNum);
        log.info("根据orderNum查询order{}", iPage);
        if (null != iPage) {
            return Result.success(iPage);
        }
        return Result.fail();
    }
}
