package com.example.jade_wine_backstage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_backstage.entity.Refund;

/**
 * <p>
 * 退款表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface RefundService extends IService<Refund> {

}
