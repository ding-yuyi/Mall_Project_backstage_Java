package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.entity.SkuKey;
import com.example.jade_wine_backstage.mapper.SkuKeyMapper;
import com.example.jade_wine_backstage.service.SkuKeyService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sku属性键表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
public class SkuKeyServiceImpl extends ServiceImpl<SkuKeyMapper, SkuKey> implements SkuKeyService {

}
