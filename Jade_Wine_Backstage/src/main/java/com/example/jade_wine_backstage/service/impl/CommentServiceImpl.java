package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.entity.Comment;
import com.example.jade_wine_backstage.mapper.CommentMapper;
import com.example.jade_wine_backstage.service.CommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * spu商品表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

}
