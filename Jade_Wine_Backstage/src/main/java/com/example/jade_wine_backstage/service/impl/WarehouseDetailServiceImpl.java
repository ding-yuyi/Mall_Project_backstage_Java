package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.entity.WarehouseDetail;
import com.example.jade_wine_backstage.mapper.WarehouseDetailMapper;
import com.example.jade_wine_backstage.service.WarehouseDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 仓库中间表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
public class WarehouseDetailServiceImpl extends ServiceImpl<WarehouseDetailMapper, WarehouseDetail> implements WarehouseDetailService {

}
