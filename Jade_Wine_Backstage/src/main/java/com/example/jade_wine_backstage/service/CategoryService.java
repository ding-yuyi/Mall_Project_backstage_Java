package com.example.jade_wine_backstage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_backstage.entity.Category;

/**
 * <p>
 * 类别表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface CategoryService extends IService<Category> {

}
