package com.example.jade_wine_backstage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.entity.GoodsSpu;

/**
 * <p>
 * spu商品表 服务类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface GoodsSpuService extends IService<GoodsSpu> {

    /**
     * 查询所有获取所有spuName
     */
    public Result queryAllSpuName();
}
