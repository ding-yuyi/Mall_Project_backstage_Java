package com.example.jade_wine_backstage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.controller.form.PageForm;
import com.example.jade_wine_backstage.entity.GoodsSku;
import com.example.jade_wine_backstage.mapper.GoodsSkuMapper;
import com.example.jade_wine_backstage.service.GoodsSkuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * sku属性键表 服务实现类
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Service
@Slf4j
public class GoodsSkuServiceImpl extends ServiceImpl<GoodsSkuMapper, GoodsSku> implements GoodsSkuService {

    @Resource
    private GoodsSkuMapper goodsSkuMapper;

    /**
     * 分页查询所有商品
     * 参数: pageNum 页码   pageSize 页容量
     */

    @Override
    public Result pageQueryAllGoodsSku(PageForm form) {
        log.info("分页查询所有商品{}", form);
        QueryWrapper<GoodsSku> wrapper = new QueryWrapper<>();
        Page<GoodsSku> page = new Page<>(form.getPageNum(), form.getPageSize());
        Page<GoodsSku> goodsSkuPage = goodsSkuMapper.selectPage(page, wrapper);
        log.info("分页查询所有商品{}", goodsSkuPage);
        if (null != goodsSkuPage) {
            return Result.success(goodsSkuPage);
        }
        return Result.fail();
    }

    /**
     * 根据id删除goods_sku
     * 参数：skuId
     */

    @Override
    public Result deleteGoods(Integer skuId) {
        log.info("根据id删除good{}", skuId);
        int i = goodsSkuMapper.deleteById(skuId);
        log.info("根据id删除good{}", i);
        if (i == 1) {
            return Result.success();
        }
        return Result.fail();
    }

    /**
     * 根据skuId修改goods_sku
     */
    @Override
    public Result updateGoodsSkuById(GoodsSku goodsSku) {
        log.info("根据id修改商品{}", goodsSku);
        int i = goodsSkuMapper.updateById(goodsSku);
        log.info("根据id修改商品{}", i);
        if (i == 1) {
            return Result.success();
        }
        return Result.fail();
    }

    @Override
    public Result insertGoods(GoodsSku goodsSku) {
        int insert = goodsSkuMapper.insert(goodsSku);
        if (insert == 1) {
            return Result.success();
        }
        return Result.fail();
    }
}
