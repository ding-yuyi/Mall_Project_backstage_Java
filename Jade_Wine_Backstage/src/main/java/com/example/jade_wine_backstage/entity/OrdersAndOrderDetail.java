package com.example.jade_wine_backstage.entity;

import lombok.Data;

@Data
public class OrdersAndOrderDetail {
    //orderId
    private Integer orderId;
    //userId
    private Integer userId;
    //订单号
    private String orderNum;
    //订单状态
    private Integer orderState;
    //    addressId
    private Integer addressId;
    //    总金额
    private Integer orderTotal;
    //    orderDetailId
    private Integer ordersDetailId;
    //    skuId
    private Integer skuId;
    //    商品数量
    private Integer skuNum;
    //    spuId
    private Integer spuId;
    //    商品类别
    private String skuAttributeSpecs;
    //    商品规格
    private String skuKey;
    //    商品价格
    private Double spuPrice;
    //    库存
    private Integer spuStock;
    //    categoryId
    private Integer categoryId;
    //    商品名
    private String spuName;
    //    {}
    private String spuAttributeList;
    //    产地
    private String spuOriginPlace;
    //    品牌
    private String spuBrand;
    //    商品描述
    private String spuDescription;
    //    父级分类
    private Integer parentId;
    //    分类名称
    private String categoryName;
}