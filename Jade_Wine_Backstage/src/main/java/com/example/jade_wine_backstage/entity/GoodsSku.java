package com.example.jade_wine_backstage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * sku属性键表
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("goods_sku")
public class GoodsSku implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "sku_id", type = IdType.AUTO)
    private Integer skuId;

    /**
     * 分类表主键
     */
    private Integer categoryId;

    /**
     * 商品表主键
     */
    private Integer spuId;

    private String skuName;

    /**
     * json字符串
     */
    private String skuAttributeSpecs;

    private String src;

    /**
     * 选项名称
     */
    private String skuKey;

    /**
     * '商品价格'
     */
    private BigDecimal spuPrice;

    /**
     * 商品总库存数量
     */
    private Long spuStock;

    /**
     * 记录添加时间
     */
    private Date rawAddTime;

    /**
     * 记录更新时间
     */
    private Date rawUpdateTime;


}
