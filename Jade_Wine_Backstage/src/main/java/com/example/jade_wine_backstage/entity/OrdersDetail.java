package com.example.jade_wine_backstage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 订单详情表
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("orders_detail")
public class OrdersDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "orders_detail_id", type = IdType.AUTO)
    private Integer ordersDetailId;

    /**
     * sku表主键
     */
    private Integer skuId;

    /**
     * 商品数量
     */
    private Integer skuNum;

    /**
     * 订单表id
     */
    private Integer orderId;

    /**
     * 记录添加时间
     */
    private Date rawAddTime;

    /**
     * 记录更新时间
     */
    private Date rawUpdateTime;


}
