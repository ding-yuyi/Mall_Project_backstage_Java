package com.example.jade_wine_backstage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.jade_wine_backstage.entity.Orders;
import com.example.jade_wine_backstage.entity.OrdersAndOrderDetail;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface OrdersMapper extends BaseMapper<Orders> {

    /**
     * 分页查询全部订单
     * 参数 form: pageNum 页码      pageSize 页容量
     *
     * @return
     */

    @Select("select o.order_id,user_id,order_num,order_state,address_id,order_total,orders_detail_id,gsku.sku_id,sku_num,gspu.spu_id,sku_attribute_specs,sku_key,spu_price,spu_stock,cg.category_id,spu_name,spu_attribute_list,spu_origin_place,spu_brand,spu_description,parent_id,category_name from orders o join orders_detail od on o.order_id = od.order_id join goods_sku gsku on od.sku_id = gsku.sku_id join goods_spu gspu on gsku.spu_id = gspu.spu_id join category cg on gspu.category_id = cg.category_id")
    public IPage<OrdersAndOrderDetail> pageQueryOrders(Page<OrdersAndOrderDetail> page);

    /**
     * 根据id修改订单状态
     * 参数：orderId
     */
    @Update("update orders set order_state = 2 where order_id = #{orderId}")
    public Integer updateOrderStateByOrderId(Integer orderId);

    @Select("select o.order_id,user_id,order_num,order_state,address_id,order_total,orders_detail_id,gsku.sku_id,sku_num,gspu.spu_id,sku_attribute_specs,sku_key,spu_price,spu_stock,cg.category_id,spu_name,spu_attribute_list,spu_origin_place,spu_brand,spu_description,parent_id,category_name from orders o join orders_detail od on o.order_id = od.order_id join goods_sku gsku on od.sku_id = gsku.sku_id join goods_spu gspu on gsku.spu_id = gspu.spu_id join category cg on gspu.category_id = cg.category_id where order_num = #{orderNum}")
    public IPage<OrdersAndOrderDetail> queryOrder(Page<OrdersAndOrderDetail> page, String orderNum);
}
