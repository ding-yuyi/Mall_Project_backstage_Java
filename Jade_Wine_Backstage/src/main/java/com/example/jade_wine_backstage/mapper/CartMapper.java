package com.example.jade_wine_backstage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jade_wine_backstage.entity.Cart;

/**
 * <p>
 * 购物车表 Mapper 接口
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface CartMapper extends BaseMapper<Cart> {

}
