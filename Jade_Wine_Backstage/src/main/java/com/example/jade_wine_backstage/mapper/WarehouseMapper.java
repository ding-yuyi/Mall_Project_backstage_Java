package com.example.jade_wine_backstage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jade_wine_backstage.entity.Warehouse;

/**
 * <p>
 * 仓库中间表 Mapper 接口
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface WarehouseMapper extends BaseMapper<Warehouse> {

}
