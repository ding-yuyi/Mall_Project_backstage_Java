package com.example.jade_wine_backstage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jade_wine_backstage.entity.OrdersDetail;

/**
 * <p>
 * 订单详情表 Mapper 接口
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
public interface OrdersDetailMapper extends BaseMapper<OrdersDetail> {

}
