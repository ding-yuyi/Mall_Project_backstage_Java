package com.example.jade_wine_backstage.controller;


import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.controller.form.Form;
import com.example.jade_wine_backstage.controller.form.PageForm;
import com.example.jade_wine_backstage.service.OrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@RestController
@Slf4j
@CrossOrigin("*")
public class OrdersController {

    @Resource
    private OrdersService ordersService;

    /**
     * 分页查询所有订单
     * PageForm: pageNum 页码     pageSize 页容量
     *
     * @param form
     * @return
     */
    @GetMapping("allOrders")
    public Result queryAllOrders(PageForm form) {
        log.info("分页查询所有订单入口{}", form);
        return ordersService.queryAllOrders(form);
    }

    /**
     * 根据id修改状态
     * 参数：orderId
     */
    @PutMapping("order")
    public Result updateOrderState(@RequestBody Form form) {
        log.info("根据id修改订单状态接口{}", form);
        return ordersService.updateOrderState(form.getOrderId());
    }

    /**
     * 根据订单号查询订单
     * 参数: orderNum 订单号
     */
    @GetMapping("order")
    public Result queryOrderByOrderNum(String orderNum) {
        log.info("根据订单号查询订单{}", orderNum);
        return ordersService.queryOrderByOrderNum(orderNum);
    }
}

