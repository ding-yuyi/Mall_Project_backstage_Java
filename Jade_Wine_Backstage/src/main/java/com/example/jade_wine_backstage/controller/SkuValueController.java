package com.example.jade_wine_backstage.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * sku属性值表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@RestController
@RequestMapping("/jade_wine_backstage/sku-value")
public class SkuValueController {

}

