package com.example.jade_wine_backstage.controller.form;

import lombok.Data;

/**
 * @PackageName:
 * @ClassName: FindUserForm
 * @Description:
 * @author: 丁予一
 * @date: 2021/7/15 21:11
 */
@Data
public class FindUserForm {
    private String username;
    //
    private Integer pageCount;
    //每页的数量
    private Integer pageSize;

    private Boolean isManager;
}
