package com.example.jade_wine_backstage.controller;


import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.controller.form.InsertProduct;
import com.example.jade_wine_backstage.controller.form.PageForm;
import com.example.jade_wine_backstage.controller.form.UpdateGoodsSkuBySkuIdForm;
import com.example.jade_wine_backstage.entity.GoodsSku;
import com.example.jade_wine_backstage.service.GoodsSkuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * sku属性键表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@RestController
@Slf4j
public class GoodsSkuController {

    @Resource
    private GoodsSkuService goodsSkuService;

    /**
     * 分页查询所有商品
     * 参数: pageNum 页码   pageSize 页容量
     */

    @GetMapping("allGoods")
    public Result pageQueryGoods(PageForm form) {
        log.info("分页查询所有商品接口{}", form);
        return goodsSkuService.pageQueryAllGoodsSku(form);
    }

    /**
     * 根据id删除good
     * 参数： skuId
     */
    @DeleteMapping("good")
    public Result deleteGood(Integer skuId) {
        log.info("根据id删除good接口{}", skuId);
        return goodsSkuService.deleteGoods(skuId);
    }

    /**
     * 根据skuId对goods_sku进行更改
     * 参数：skuName 商品名   spuStock 库存     spuPrice 价格     skuAttributeSpecs 属性
     */
    @PutMapping("goods")
    public Result updateGoodsSku(@RequestBody UpdateGoodsSkuBySkuIdForm form) {
        log.info("修改商品入口{}", form);
        GoodsSku goodsSku = new GoodsSku();
        BeanUtils.copyProperties(form, goodsSku);
        log.info("goosSku{}", goodsSku);
        return goodsSkuService.updateGoodsSkuById(goodsSku);
    }

    @PostMapping("goods")
    public Result insertGoods(@RequestBody InsertProduct insertProduct) {
        log.info("新增商品入口{}", insertProduct);
        GoodsSku goodsSku = new GoodsSku();
        BeanUtils.copyProperties(insertProduct, goodsSku);
        System.out.println();
        return goodsSkuService.insertGoods(goodsSku);
    }
}

