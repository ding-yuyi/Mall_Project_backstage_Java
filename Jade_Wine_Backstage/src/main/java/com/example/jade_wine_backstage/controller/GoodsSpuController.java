package com.example.jade_wine_backstage.controller;


import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.service.GoodsSpuService;
import com.example.jade_wine_backstage.util.QiniuUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * <p>
 * spu商品表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@RestController
@Slf4j
public class GoodsSpuController {

    @Resource
    private GoodsSpuService goodsSpuService;

    /**
     * 查询goodsSpu获得所有spu_name
     */
    @GetMapping("goodsSpu")
    public Result querySpuName() {
        log.info("获取所有spuName入口");
        return goodsSpuService.queryAllSpuName();
    }

    /**
     * 上传文件功能
     */
    @RequestMapping("upload")
    public Result upload(@RequestParam("file") MultipartFile file) {
        FileInputStream iut = null;
        String filePath = "";
        String src = "";
        try {
            String fileName = file.getOriginalFilename();
            if (fileName.indexOf("\\") != -1) {
                fileName = fileName.substring(fileName.lastIndexOf("\\"));
            }
            // 获取文件存放地址
            filePath = "/img";
            File f = new File(filePath);
            if (!f.exists()) {
                f.mkdirs();// 不存在路径则进行创建
            }
            // 重新自定义文件的名称
            filePath = filePath + fileName;
            iut = (FileInputStream) file.getInputStream();
            QiniuUtil.upload(filePath, iut);
            src = QiniuUtil.fullPath(filePath);
            Thread.sleep(10000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (iut != null) {
                try {
                    iut.close();
                    return Result.success(src);
                } catch (IOException e) {
                    e.printStackTrace();
                    return Result.fail();
                }
            }
        }
        return null;
    }
}

