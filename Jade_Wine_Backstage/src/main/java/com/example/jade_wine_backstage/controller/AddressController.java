package com.example.jade_wine_backstage.controller;


import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.service.AddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 用户地址表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@RestController
@Slf4j
@CrossOrigin("*")
public class AddressController {

    @Resource
    private AddressService addressService;

    /**
     * 根据UserId查询地址
     * 参数: userId 用户Id
     *
     * @param addressId
     * @return
     */

    @GetMapping("address")
    public Result queryAddressByUserId(Integer addressId) {
        log.info("查询地址接口{}", addressId);
        return addressService.queryAddressByUserId(addressId);
    }

}

