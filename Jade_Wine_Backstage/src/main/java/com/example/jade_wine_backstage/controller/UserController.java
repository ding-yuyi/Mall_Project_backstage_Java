package com.example.jade_wine_backstage.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.jade_wine_backstage.common.ControllerBase;
import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.controller.form.FindUserForm;
import com.example.jade_wine_backstage.controller.form.LoginForm;
import com.example.jade_wine_backstage.entity.User;
import com.example.jade_wine_backstage.service.UserService;
import com.example.jade_wine_backstage.util.EncryptUtil;
import com.example.jade_wine_backstage.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@RestController
@RequestMapping("user")
@Slf4j
public class UserController extends ControllerBase {

    @Resource
    private UserService userService;

    @GetMapping("user")
    public Result login(HttpServletResponse response, @Valid LoginForm form, BindingResult result) {
        //打印用户信息
        log.info("用户信息:{}", form);
        //对密码加密
        form.setPassword(EncryptUtil.encrypt(form.getPassword()));

        Result validResult = extractError(result);
        if (validResult != null) {
            return validResult;
        }

        User user = userService.login(form);
        log.info("返回结果，{}", user);
        if (null == user) {
            return Result.fail("登录失败");
        }
        String jwt = JwtUtils.createJWT(user);
        log.info("jwt：{}", jwt);
        response.setHeader(JwtUtils.AUTH_TOKEN_NAME, jwt);
        HashMap<String, Object> userInfo = new HashMap<>();
        userInfo.put("auth_token", jwt);
        userInfo.put("loginUser", user);
        return Result.success(userInfo);
    }

    //查询普通用户
    @GetMapping("users")
    public Result findNormalUser(FindUserForm form) {
        log.info("用户,{}", form);

        Page<User> userPage = userService.findUser(form);

        if (null == userPage.getRecords()) {
            return Result.fail("查询失败，请重试");
        }
        return Result.success(userPage);
    }

    //移除账户
    @DeleteMapping("users")
    public Result deleteUser(Integer userId) {
        log.info("用户id,{}", userId);
        Boolean flag = userService.deleteUser(userId);
        if (flag) {
            return Result.success("移除成功");
        }
        return Result.fail("移除失败");
    }

    //禁用账户
    @PutMapping("manager")
    public Result forbiddenUser(Integer userId) {
        log.info("用户id,{}", userId);
        Boolean flag = userService.forbiddenUser(userId);
        if (flag) {
            return Result.success("禁用成功");
        }
        return Result.fail("禁用失败");
    }

}

