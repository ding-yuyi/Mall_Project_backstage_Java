package com.example.jade_wine_backstage.controller;


import com.example.jade_wine_backstage.common.Result;
import com.example.jade_wine_backstage.service.UserDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 用户信息表 前端控制器
 * </p>
 *
 * @author cyx
 * @since 2021-07-14
 */
@RestController
@CrossOrigin("*")
@Slf4j
public class UserDetailController {

    @Resource
    private UserDetailService userDetailService;

    @GetMapping("userDetail")
    public Result queryUserDetailByUserId(Integer userId) {
        log.info("获得昵称{}", userId);
        return userDetailService.queryNickNameByUserId(userId);
    }
}

