package com.example.jade_wine_backstage.controller.form;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class InsertProduct {
    private Integer categoryId;
    private Integer spuId;
    private String skuName;
    private String skuAttributeSpecs;
    private String src;
    private String skuKey;
    private BigDecimal spuPrice;
    private Long spuStock;
}
