package com.example.jade_wine_backstage.controller.form;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UpdateGoodsSkuBySkuIdForm {
    private Integer skuId;
    private String skuName;
    private Long spuStock;
    private BigDecimal spuPrice;
    private String skuAttributeSpecs;
}
