package com.example.jade_wine_backstage.controller.form;

import lombok.Data;

@Data
public class PageForm {
    private Integer pageNum;
    private Integer pageSize;
}
