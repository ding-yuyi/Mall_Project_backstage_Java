package com.example.jade_wine_backstage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.jade_wine_backstage.mapper")
public class JadeWineBackstageApplication {

    public static void main(String[] args) {
        SpringApplication.run(JadeWineBackstageApplication.class, args);
    }

}
